/** reespirator_sniffer.cpp
 *
 * HMI v1 frame types sniffer.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */


#include "reespirator_sniffer.hpp"

namespace reespirator
{

void log_bytes(const char* src_id, const u8* bytes, u8 count);

u8 buffer[FRAME_MAX_SIZE * 10];

bool snif(HardwareSerial& serial_in, FrameBuffer& fb)
{
    if (serial_in.available())
    {
        size_t read_bytes = serial_in.readBytes(buffer, sizeof(buffer));

        log_bytes(fb.src_id, buffer, read_bytes);

        return true;
    }

    return false;
}

void dump_buffer(FrameBuffer& buffer)
{
}

void log_bytes(const char* src_id, const u8* bytes, u8 count)
{
    Serial.print(src_id);
    Serial.print(' ');
    Serial.print(millis());
    Serial.print(' ');
    Serial.print(count);
    Serial.print('[');
    while (count-- > 0)
    {
        u8 byte = *bytes++;
        if (byte < 0x10) Serial.print('0');
        Serial.print(byte, 16);
    }
    Serial.print(']');
    Serial.println();
}

}; // namespace reespirator
