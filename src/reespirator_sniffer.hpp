/** reespirator_sniffer.hpp
 *
 * HMI v1 frame types sniffer.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @license GNU General Public License v3.0
 *
 */

#pragma once

#include <reespirator/SerialMonitor/Hmi01Frames.hpp>
#include <reespirator/Core/Types/Integers.hpp>
#include <Arduino.h>

#define FRAME_TIMEOUT_MILLIS 5

namespace reespirator
{

struct FrameBuffer
{
    u8 buffer[FRAME_MAX_SIZE];
    u8 size;
    const char* src_id;
    FrameBuffer(const char* src_id) : src_id(src_id) { size = 0; }
    size_t freeBytes() { return FRAME_MAX_SIZE - size; }
};

bool snif(HardwareSerial& serial_in, FrameBuffer& fb);
void dump_buffer(FrameBuffer& fb);

}; // namespace reespirator

