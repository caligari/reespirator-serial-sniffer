
#include <Arduino.h>
#include <reespirator_sniffer.hpp>

#define SM_TX_PIN GPIO_NUM_16
#define DC_TX_PIN GPIO_NUM_17

using namespace reespirator;

HardwareSerial SerialSM(1); // Serial to SerialMonitor (UART1)
HardwareSerial SerialDC(2); // Serial to DisplayController (UART2)

void setup()
{
    // UART0: standard output to PC (Serial-USB)
    Serial.begin(115200);
    Serial.println("Reespirator sniffer v1.0.0");

    // UART1: serial settings for SerialMonitor
    SerialSM.begin(115200, SERIAL_8N1, SM_TX_PIN);

    // UART2: serial settings for DisplayController
    SerialDC.begin(115200, SERIAL_8N1, DC_TX_PIN);
}

void loop()
{
    FrameBuffer buffer1("SM"); // frame buffer for SerialMonitor
    FrameBuffer buffer2("DC"); // frame buffer for DisplayController

    if (snif(SerialSM, buffer1)) dump_buffer(buffer1);
    if (snif(SerialDC, buffer2)) dump_buffer(buffer2);
}

