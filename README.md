# Reespirator SerialMonitor sniffer 

This project snifs transmissions between [Reespirator firmware](https://gitlab.com/reespirator/firmware/arduino-2020) using [SerialMonitor protocol](https://docs.google.com/document/d/1lItbWZhYFjCUJKEzwG3V0N3ZbFNCW4r7WvXlSnQcjlk/edit#heading=h.pncd0m3mo5yw) and a display controller like [Display 2020](https://gitlab.com/reespirator/display/display-2020).

Requirements:

- An ESP32 board, with USB connection to PC (optional, you could use a USB-UART adapter).
- [Platformio Core](https://docs.platformio.org/en/latest/core/installation.html) to flash and monitor via serial transmissions.

## ESP32 Firmware

Download latest release from [this repo](https://gitlab.com/caligari/reespirator-serial-sniffer/-/releases):

Firmware to be flashed in ESP32. Use Platformio to build and flash into ESP32:

    pio run -t upload

## Connections

| ESP32 | Reespirator Board | Notes |
|:-----:|:-----------------:|-------|
| IO16 | TXD | Connect with 1K resistor |
| IO17 | RXD | Connect with 1K resistor |
| GND | GND | |

You could change ESP32 pins by modifying [src/main.cpp](src/main.cpp) (_SM\_TX\_PIN_ and _DC\_TX\_PIN_ definitions).

## Sniffer

To show frame transmissions between Display 2020 and Reespirator firmware:

    pio device monitor

## Inject frames to reespirator-2020 firmware (GNU/Linux)

It is useful if you do not have a display controller connected to Reespirator firmware or you want to debug the serial monitor communications.

You must have access to serial device in your PC where Reespirator firmware is connected to (probably _/dev/ttyACM0_ or _/dev/ttyUSB1_ if you are connected via USB). Review that your user has read and write permissions to the device (usually, belonging to the _plugdev_ or _dialout_ group or, eventually not recommended, executing below commands as root with _sudo_).

Set up the serial port parameters:

    stty -F /dev/ttyACM0 115200 raw -echo -echoe -echok

Inject frames from hexadecimal byte string:

    echo '0x55 0x01 0x01 0x01 0x05 0xb9 0xa3' | xxd -r > /dev/ttyACM0

_xxd_ is a command line utility to convert between hex streams to binary and viceversa. Install on your favourite GNU/Linux distro (Debian: ```sudo apt install xxd```)
